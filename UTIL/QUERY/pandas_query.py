# -*- coding=utf-8 -*-
import pandas as pd
from query_data import query_corpus
import cPickle

def get_datetime(corpus, article_id):
    datetime_obj= pd.to_datetime(corpus.open(corpus.fileids()[article_id]). \
                                 read().split('\n')[2],
                                 format='%Y/%m/%d:%W %H:%M:%S')
    return datetime_obj


if __name__ == "__main__":
    corpus_name = 's_1000'
    corpus = query_corpus(corpus_name)
    article_dict = {}
    for i, x in enumerate(corpus.fileids()):
        print 'Proccess article:%i' %(i)
        article_dict[x] = get_datetime(corpus, i)
    cPickle.dump(article_dict, open('article_dict', 'wb'), -1)
