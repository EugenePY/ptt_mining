# -*- coding=utf-8 -*-
import sys
import os
from nltk.corpus.reader.plaintext import PlaintextCorpusReader as reader


def query_corpus(query_corpus, article_id=None):
    print "Current Working Dir: %s" % (os.getcwd())

    sub_courpus_dir = query_corpus
    corpus_dir = '../../DATA/nfs/nas-4.1/yhhuang/ptt/' + sub_courpus_dir
    """
    try:
        f = open("{corpus_reader}.pkl".format(corpus_reader=sub_courpus_dir),
                 'rb')
        print "Found the Corpus Reader Class: %s" % (f.name)
        new_corpus = data.pickle.load(f)
        print "Loading Done."

    except IOError:
    """
    # print "Corpus Reader not found, Creating the New Reader: %s.pkl" % \
    #     (sub_courpus_dir)
    new_corpus = reader(corpus_dir, '.*')
    # data.pickle.dump(new_corpus, open(sub_courpus_dir+'.pkl', 'wb'), 2)

    corpus = new_corpus

    if article_id is None:
        print "No specific the Article Id, Do nothing"
        return corpus
    else:
        queried = [corpus.open(x).read().strip() for x in corpus.fileids()
                   if article_id in x]
        return [corpus, queried]


if __name__ == "__main__":
    if len(sys.argv) == 2:
        query_corpus(sys.argv[1])
    elif len(sys.argv) == 3:
        result = query_corpus(sys.argv[1], sys.argv[2])[1]
        for article in result:
            print article
    else:
        print 'Ileagal input'
